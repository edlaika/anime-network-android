package animenetworks.animenetworks;


import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;


/**
 * A simple {@link Fragment} subclass.
 */

public class ThirdFragment extends Fragment {

    private static final String Video_url = "https://dl.dropboxusercontent.com/u/102136471/remove/Dragon%20Ball%20Z%20Opening%201%20-%20Original%201989%20Japanese%20%281080p%20HD%29.mp4";
    public ThirdFragment() {

        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_third, container, false);

// Inflate the layout for this fragment
        VideoView view = (VideoView) v.findViewById(R.id.videoView2);
        Uri video = Uri.parse(Video_url);

        view.setMediaController( new MediaController(getActivity()));
        view.setVideoURI(video);
        view.start();
        view.requestFocus();

        return v;

    }


}
