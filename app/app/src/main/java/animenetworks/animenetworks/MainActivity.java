package animenetworks.animenetworks;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Edgar and Ahmed.
 */
public class MainActivity extends ActionBarActivity {

    DatabaseHelper databaseHelper = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;

    }

    public void onClick(View v)
    {
        if(v.getId() == R.id.login) {
            EditText username = (EditText)findViewById(R.id.username);
            String string = username.getText().toString();
            EditText passwordField = (EditText)findViewById(R.id.password);
            String pass = passwordField.getText().toString();

            String password = databaseHelper.searchPassword(string);
            //Check if the password matches or not.
            if(pass.equals(password)) {
                Intent intent = new Intent(MainActivity.this, WelcomeScreen.class);
                intent.putExtra("Username", string);
                startActivity(intent);
            }
            else {
                Toast toast = Toast.makeText(MainActivity.this , "Learn how to spell and check again -_-!" , Toast.LENGTH_SHORT);
                toast.show();
            }



        }
        if(v.getId() == R.id.signup) {
            Intent intent = new Intent(MainActivity.this, Register.class);
            startActivity(intent);

        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
//Check settings menu and display extra options.
            case R.id.suggestion:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "edax94@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Anime Network's In-app support");

                emailIntent.putExtra(Intent.EXTRA_TEXT, "Please specify your suggestion or problem!");
                startActivity(Intent.createChooser(emailIntent, "Send email with..."));
                break;
            case R.id.rate:
                Intent ratewebIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Edgar+Axelsson"));
                startActivity(ratewebIntent);
                break;
            case R.id.facebook:
                String facebookUrl = "https://www.facebook.com/pages/Team-Husky/347547118781638";
                try {
                    int versionCode = getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
                    if (versionCode >= 3002850) {
                        Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    } else {
                        Uri uri = Uri.parse("fb://page/347547118781638");
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
                }
            case R.id.wiki:
                Intent wikiIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://phqu.t15.org/wiki/wiki/"));
                startActivity(wikiIntent);
                break;


        }

        return super.onOptionsItemSelected(item);
    }

}


