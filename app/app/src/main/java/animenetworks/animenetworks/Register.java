package animenetworks.animenetworks;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Edgar and Ahmed.
 */
public class Register extends Activity {

    DatabaseHelper helper = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
    }

    public void onClick(View v) {
        if(v.getId()== R.id.Bsignupbutton) {
            EditText name = (EditText)findViewById(R.id.name);
            EditText email = (EditText)findViewById(R.id.email);
            EditText username = (EditText)findViewById(R.id.username);
            EditText password1 = (EditText)findViewById(R.id.password1);
            EditText password2 = (EditText)findViewById(R.id.password2);

            String nameString = name.getText().toString();
            String emailString = email.getText().toString();
            String usernameString = username.getText().toString();
            String password1String = password1.getText().toString();
            String password2String = password2.getText().toString();

            if(!password1String.equals(password2String)) {
                Toast toast = Toast.makeText(Register.this , "Your password is a cheeky nandos m8!" , Toast.LENGTH_SHORT);
                toast.show();
            }
            else {
                Toast toast = Toast.makeText(Register.this , "You deserve a cookie m8 congrats!" , Toast.LENGTH_SHORT);
                toast.show();


                //insert data to database.
                Info info = new Info();
                info.setName(nameString);
                info.setEmail(emailString);
                info.setUsername(usernameString);
                info.setPass(password1String);


                helper.insertInfo(info);
                finish();
            }

        }

    }


}
