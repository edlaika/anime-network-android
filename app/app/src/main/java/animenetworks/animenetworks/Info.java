package animenetworks.animenetworks;

/**
 * Created by Edgar and Ahmed.
 */
public class Info {
    String name ,email,username,pass;

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return this.username;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    public String getPass() {
        return this.pass;
    }

}