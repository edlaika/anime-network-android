package animenetworks.animenetworks;

import android.app.ActionBar;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edgar and Ahmed.
 */
public class ChatArrayAdapter extends ArrayAdapter<MessageProvider> {
  private  List<MessageProvider> chat_list = new ArrayList<MessageProvider>();
    private TextView chatText;
    Context context;

    public ChatArrayAdapter(Context context, int resource) {
        super(context, resource);
        this.context = context;
    }

    @Override
    public void add(MessageProvider object) {
        chat_list.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return chat_list.size();
    }

    @Override
    public MessageProvider getItem(int position) {
        return chat_list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView= inflator.inflate(R.layout.single_message_layout, parent, false);
        }
        chatText = (TextView) convertView.findViewById(R.id.singleMessage);
        String Message;
        boolean pos;
        // Get the message.
        MessageProvider provider = getItem(position);
        Message = provider.message;
        pos = provider.postion;
        // Set the text and display its specific colour as well as set position.
        chatText.setText(Message);
        chatText.setBackgroundResource(pos ? R.color.colorAccent : R.color.colorPrimaryDark);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ActionBar.LayoutParams.WRAP_CONTENT);
        if(!pos) {
            params.gravity = Gravity.RIGHT;
        }else {
            params.gravity = Gravity.LEFT;
        }
        chatText.setLayoutParams(params);

        return convertView;
    }
}
