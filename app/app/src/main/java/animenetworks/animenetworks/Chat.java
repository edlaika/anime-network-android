package animenetworks.animenetworks;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class Chat extends Activity {
    ListView listView;
    EditText chat_text;
    Button send;
    boolean position = false;
    ChatArrayAdapter adapter;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        listView = (ListView) findViewById(R.id.chatlistView);
        chat_text = (EditText) findViewById(R.id.chatText);
        send = (Button) findViewById(R.id.send_button);
        adapter = new ChatArrayAdapter(context, R.layout.single_message_layout);
        listView.setAdapter(adapter);
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                // set button position for the list view.
                listView.setSelection(adapter.getCount() - 1);
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.add(new MessageProvider(position, chat_text.getText().toString()));
                position = !position;
                chat_text.setText("");

            }
        });
    }
}
