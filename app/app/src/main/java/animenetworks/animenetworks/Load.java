package animenetworks.animenetworks;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Edgar and Ahmed.
 */
public class Load extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load);
        //Put the application in a sleeping stage for 3 seconds.
        Thread thread = new Thread(){
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    Intent intent = new Intent("load");
                    startActivity(intent);
                }
            }
        };
        thread.start();


    }
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}

