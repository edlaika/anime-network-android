package animenetworks.animenetworks;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


/**
 * Created by Edgar on 2015-11-18.
 */
public class WelcomeScreen extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_welcome_screen);
        String username = getIntent().getStringExtra("Username");

        TextView tv = (TextView)findViewById(R.id.username);
        tv.setText(username);

    }
    // Open new activity after the welcome screen.
    public void buttonClick(View v)  {
        if(v.getId() == R.id.work) {
            Intent intent = new Intent(WelcomeScreen.this, Menu_main.class);
            startActivity(intent);

        }
    }
    public void clickChat(View v)  {
        if(v.getId() == R.id.pls) {
            Intent intent = new Intent(WelcomeScreen.this, Chat.class);
            startActivity(intent);

        }
    }

}