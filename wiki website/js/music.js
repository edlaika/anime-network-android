var collection=[];
var loadedIndex=0;
function init(audios) {
  for(var i=0;i<audios.length;i++) {
    var audio = new Audio(audios[i]);
    collection.push(audio);
    buffer(audio);
  }
}


function buffer(audio) {
  if(audio.readyState==4)return loaded();
  setTimeout(function(){buffer(audio)},100);
}


function loaded() {
  loadedIndex++;
  if(collection.length==loadedIndex)playLooped();
}


function playLooped() {
  var audio=Math.floor(Math.random() * (collection.length));
  audio=collection[audio];
  audio.play();
  setTimeout(playLooped,audio.duration*1000);
}
// Paste your music url here make sure its .ogg format or else it wont work!
// Can use dropbox to host or local by making a music folder.
init([
  'https://dl.dropboxusercontent.com/u/102136471/remove/1.wav',
  'https://dl.dropboxusercontent.com/u/102136471/remove/2.wav',
    'https://dl.dropboxusercontent.com/u/102136471/remove/3.wav',


]);
